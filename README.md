## Five different machine learning algorithms to classify The Cancer Genome Atlas (TCGA) Pan-Cancer data

In this project I implemented 5 different machine learning algirithms to classify the TCGA project data. 


Only 5 types of cancer were considered for the task including breast carcinoma (BRCA), kidney renal clear-cell carcinoma (KIRC), lung adenocarcinoma (LUAD), colon adenocarcinoma (COAD), and prostate adenocarcinoma (PRAD).


- Implementation
All the scripts available in the current directory were developed in Python 3.8 language. Various non-standard Python libraries including NumPy, Pandas, Matplotlib, and Scikit-learn were used in the suite.

Start by creating a new anaconda environment, it is always better to do so to avoid the conflicts between the different libraries.

Open the terminal and run the following command: 

`conda create -n myenv python=3.8`


To install the libraries:

`pip install Pandas`

`pip install numpy`

`pip install scikit-learn`

## Multinomial logistic regression

The algorithm is implemeted in `multinomial_regression.py` file. 
Make sure you download the dataset (https://archive.ics.uci.edu/ml/datasets/gene+expression+cancer+RNA-Seq) and run on the same terminal:

`python multinomial_regression.py`


## Decision Trees

The decision tree learning is used as predictive model that goes from the observation, represented as branches to the conclusions represented as leaves. In tree models the outcome variable can take a discrete set of values or continuous values. Trees where the outcome variables take discrete values are called classification trees. Since the TCGA dataset contains discrete target variables, we used classification trees algorithm. 
The algorithm is implemeted in `decision_tree.py` file. 
Make sure you download the dataset (https://archive.ics.uci.edu/ml/datasets/gene+expression+cancer+RNA-Seq) and run on the same terminal:

`python decision_tree.py`

## Extra-tree classfier

To improve the predictive accuracy and control overfitting of the decision tree, I implemented an extra-tree classifier.
The algorithm is implemeted in `extra_tree_classifier.py` file. 
Make sure you download the dataset (https://archive.ics.uci.edu/ml/datasets/gene+expression+cancer+RNA-Seq) and run on the same terminal:

`python extra_tree_classifier.py`


## Label Propagation 

The advantage of the label propagation algorithm is that it is less time consuming than the other algorithms and it does not require any parameter. However, this algorithm produces no unique solution, but an aggregate of many solution.

The algorithm is implemeted in `label_propagation.py` file. 
After downloading the dataset (https://archive.ics.uci.edu/ml/datasets/gene+expression+cancer+RNA-Seq) and run on the terminal:

`python label_propagation.py`

## Multi-Layer Perceptron

Multi-layer perceptron algorithm uses a supervised learning approach called backpropagation for the training. The algorithm learns by changing connection weights after each element of data is processed.  It can distinguish the data that is not linearly separable. 

The algorithm is implemeted in `multi_layer_perceptron.py` file. 
After downloading the dataset (https://archive.ics.uci.edu/ml/datasets/gene+expression+cancer+RNA-Seq) and run on the terminal:

`python multi_layer_perceptron.py`
